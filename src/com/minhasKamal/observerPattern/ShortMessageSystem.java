package com.minhasKamal.observerPattern;

public class ShortMessageSystem extends Message{
	
	protected void sendNotification(String text){
		System.out.println("SMS is sent!");
		System.out.println(text);
	}
}
