package com.minhasKamal.observerPattern;

public class VoiceCall extends Message{
	protected void sendNotification(String text){
		System.out.println("Voice call is sent!");
		System.out.println(text);
	}
}
