package com.minhasKamal.observerPattern;

public class Email extends Message{
	protected void sendNotification(String text){
		System.out.println("Email is sent!");
		System.out.println(text);
	}
}
