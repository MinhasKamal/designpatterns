package com.minhasKamal.observerPattern;

public abstract class Message {
	protected abstract void sendNotification(String text);
}
