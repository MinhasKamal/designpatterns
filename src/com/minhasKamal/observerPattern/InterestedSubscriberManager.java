package com.minhasKamal.observerPattern;

import java.util.List;

public class InterestedSubscriberManager {
	
	private List<Subscriber> subscribers;
	
	public InterestedSubscriberManager(List<Subscriber> subscribers) {
		this.subscribers = subscribers;
	}
	
	public void notifyAll(String text) {
		for(Subscriber subscriber: subscribers){
			subscriber.getMessage(text);
		}
	}
}
