package com.minhasKamal.observerPattern;

public class Subscriber {
	private String name;
	private Message messagetype;
	
	public Subscriber(String name, Message messagetype) {
		this.name = name;
		this.messagetype = messagetype;
	}
	
	public void getMessage(String text) {
		System.out.println("\n\n" + name + ": ");
		messagetype.sendNotification(text);
	}
}
