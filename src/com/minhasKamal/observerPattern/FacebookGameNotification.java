package com.minhasKamal.observerPattern;

public class FacebookGameNotification {
	private boolean isNewNotificationReady;
	private InterestedSubscriberManager interestedSubscriberManager;
	
	public FacebookGameNotification(InterestedSubscriberManager interestedSubscriberManager) {
		isNewNotificationReady = false;
		this.interestedSubscriberManager = interestedSubscriberManager;
	}
	
	public void newNotification(){
		isNewNotificationReady = true;
	}
	
	public void notifySubscribers(String text){
		if(isNewNotificationReady){
			interestedSubscriberManager.notifyAll(text);
			
			isNewNotificationReady = false;
		}else{
			System.out.println("\n\n## Sorry no new notification is created");
		}
	}
}
