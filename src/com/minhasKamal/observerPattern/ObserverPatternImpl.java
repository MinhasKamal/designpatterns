package com.minhasKamal.observerPattern;

import java.util.Arrays;

public class ObserverPatternImpl {
	public static void main(String[] args) {
		Subscriber[] subscribers = {
				new Subscriber("Abir", new Email()),
				new Subscriber("Aloy", new ShortMessageSystem()),
				new Subscriber("Ashik", new VoiceCall()),
				new Subscriber("Apon", new Email()),
				new Subscriber("Akash", new VoiceCall()),
		};
		
		InterestedSubscriberManager interestedSubscribers = new InterestedSubscriberManager(Arrays.asList(subscribers));
		
		FacebookGameNotification facebookGameNotification = new FacebookGameNotification(interestedSubscribers);
		
		facebookGameNotification.newNotification();
		facebookGameNotification.notifySubscribers("New exciting games are available.");
		
		facebookGameNotification.notifySubscribers("A new card game is available.");
		
	}
}
