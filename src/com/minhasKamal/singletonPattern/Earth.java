package com.minhasKamal.singletonPattern;

public class Earth extends Planet{
	private static Planet earth = null;
	
	private Earth(double radious, double satellite){
		this.radious = radious;
		this.satellite = satellite;
	}
	
	public double calculateArea(){
		return 3*radious*radious*radious*3.1416/4;
	}
	
	public static synchronized Planet createPlanet(){
		if(earth==null){
			earth = new Earth(1000, 1);
			return earth;
		}else{
			return earth;
		}
	}
}
