package com.minhasKamal.singletonPattern;

public class Creator implements Runnable{
	
	
	@Override
	public void run() {
		Earth earth = (Earth) Earth.createPlanet();
		System.out.println(earth + " area is: " + earth.calculateArea());
	}
	
}
