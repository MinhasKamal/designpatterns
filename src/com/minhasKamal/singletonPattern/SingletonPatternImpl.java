package com.minhasKamal.singletonPattern;

public class SingletonPatternImpl{
	public static void main(String[] args) {
		for(int i=0; i<10; i++){
			Creator creator = new Creator();
			creator.run();
		}
	}

	
}
