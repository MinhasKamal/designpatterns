package com.minhasKamal.chainOfResponsibilityPattern;

public class ExtraHardSmasher implements ISmasher{
	public void smash(int density){
		if(density<1000){
			System.out.println("Smashed by extra hard smasher.");
		}else{
			new NullSmasher().smash(density);
		}
	}
}
