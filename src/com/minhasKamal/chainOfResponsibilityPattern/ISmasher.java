package com.minhasKamal.chainOfResponsibilityPattern;

public interface ISmasher {
	public void smash(int density);
}
