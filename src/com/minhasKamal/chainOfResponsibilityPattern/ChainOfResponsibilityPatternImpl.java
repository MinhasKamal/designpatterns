package com.minhasKamal.chainOfResponsibilityPattern;

public class ChainOfResponsibilityPatternImpl {
	public static void main(String[] args) {
		int density = 750;
		
		new SoftSmasher().smash(density);
	}
}
