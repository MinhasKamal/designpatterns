package com.minhasKamal.chainOfResponsibilityPattern;

public class HardSmasher implements ISmasher{
	public void smash(int density){
		if(density<500){
			System.out.println("Smashed by hard smasher.");
		}else{
			new ExtraHardSmasher().smash(density);
		}
	}
}
