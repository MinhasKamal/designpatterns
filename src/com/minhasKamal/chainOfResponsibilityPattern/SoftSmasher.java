package com.minhasKamal.chainOfResponsibilityPattern;

public class SoftSmasher implements ISmasher{
	public void smash(int density){
		if(density<200){
			System.out.println("Smashed by soft smasher.");
		}else{
			new HardSmasher().smash(density);
		}
	}
}
