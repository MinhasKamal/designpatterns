package com.minhasKamal.adapterPattern;

public interface Shape {
	void draw(int pointx1, int pointy1, int pointx2, int pointy2);
}
