package com.minhasKamal.adapterPattern;

public class LegacyRectangle {
	public void draw(int pointx1, int pointy1, int width, int height){
		System.out.println("Rectange is drawn. \n point X1: " + pointx1 + "\n point Y1: " + pointy1 + "\n width: " + width + "\n height: " + height);
	}
}
