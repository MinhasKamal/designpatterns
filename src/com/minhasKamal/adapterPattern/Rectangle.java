package com.minhasKamal.adapterPattern;

public class Rectangle implements Shape{
	@Override
	public void draw(int pointx1, int pointy1, int pointx2, int pointy2){
		new RectangleAdapter().drawAdapter(pointx1, pointy1, pointx2, pointy2);
	}
}
