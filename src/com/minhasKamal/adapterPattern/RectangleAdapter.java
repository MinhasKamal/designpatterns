package com.minhasKamal.adapterPattern;

public class RectangleAdapter {
	public void drawAdapter(int pointx1, int pointy1, int pointx2, int pointy2){
		new LegacyRectangle().draw(pointx1, pointy1, pointx2-pointx1, pointy2-pointy1);
	}
}
