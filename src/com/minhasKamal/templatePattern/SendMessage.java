package com.minhasKamal.templatePattern;

public abstract class SendMessage {
	public SendMessage() {
		connectToServer();
		message();
		disConnectFromServer();
	}

	private void connectToServer() {
		System.out.println("Connecting...");
		System.out.println("Connected!");
	}
	
	protected abstract void message();
	
	private void disConnectFromServer() {
		System.out.println("Connection ended!");
	}
}