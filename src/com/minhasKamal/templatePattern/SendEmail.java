package com.minhasKamal.templatePattern;

public class SendEmail extends SendMessage{
	public SendEmail() {
		super();
	}
	
	protected void message(){
		System.out.println("Email is sent!!!");
	}
}
