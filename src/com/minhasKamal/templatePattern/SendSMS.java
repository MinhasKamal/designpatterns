package com.minhasKamal.templatePattern;

public class SendSMS extends SendMessage{
	public SendSMS() {
		super();
	}
	
	protected void message(){
		System.out.println("SMS is sent!!!");
	}
}
