package com.minhasKamal.compositePattern;

public abstract class PrimaryGate implements IGate{
	protected String gateName;
	
	public PrimaryGate() {
		this.gateName = null;
	}
	
	public String tostring(){
		return gateName;
	}
}
