package com.minhasKamal.compositePattern;

import java.util.ArrayList;

public class SecondaryGate extends PrimaryGate{
	protected ArrayList<IGate> gates;
	
	public SecondaryGate(String gateName) {
		super.gateName = gateName;
		this.gates = new ArrayList<>();
	}
	
	public void add(IGate gate){
		gates.add(gate);
	}
	
	public void remove(IGate gate){
		gates.remove(gate);
	}
	
	public void printAllPrimaryGates(){
		System.out.println("##" + gateName + " consists of:");
		for(IGate gate: gates){
			System.out.println(gate.tostring());
		}

		System.out.println("\n\n");
	}
}
