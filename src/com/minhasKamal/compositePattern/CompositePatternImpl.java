package com.minhasKamal.compositePattern;

public class CompositePatternImpl {
	public static void main(String[] args) {
		SecondaryGate nandGate = new SecondaryGate("NandGate");
		nandGate.add(new AndGate());
		nandGate.add(new NotGate());
		nandGate.printAllPrimaryGates();
		
		
		SecondaryGate norGate = new SecondaryGate("NorGate");
		norGate.add(new OrGate());
		norGate.add(new NotGate());
		norGate.printAllPrimaryGates();
		
	}
}
