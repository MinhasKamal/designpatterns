package com.minhasKamal.strategyPattern;

public class StrategyPatternImpl {
	public static void main(String[] args) {
		Tom tom = new Tom();
		
		Expression e;
		//e = new Expression1();
		e = new Expression2();
		
		tom.like(e);
	}
}
