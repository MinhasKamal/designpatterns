package com.minhasKamal.strategyPattern;

public class Expression2 extends Expression{
	public void execute(){
		laugh();
		kneelDown();
		sayYo();
		giveThanks();
		flipEye();
	}
	
	public void laugh(){
		System.out.println("Laugh out loud");
	}
	
	public void kneelDown(){
		System.out.println("Kneel down");
	}
	
	public void sayYo(){
		System.out.println("Say yo");
	}
	
	public void giveThanks(){
		System.out.println("Give funny thanks");
	}
	
	public void flipEye(){
		System.out.println("Flip eye once");
	}
}
