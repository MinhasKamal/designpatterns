package com.minhasKamal.strategyPattern;

public class Expression1 extends Expression{
	public void execute(){
		smile();
		jump();
		raisehand();
		giveThanks();
		flipEye();
	}
	
	public void smile(){
		System.out.println("Smile gently");
	}
	
	public void jump(){
		System.out.println("Jump high");
	}
	
	public void raisehand(){
		System.out.println("Raise both hand");
	}
	
	public void giveThanks(){
		System.out.println("Give sweet thanks");
	}
	
	public void flipEye(){
		System.out.println("flip eye twice");
	}
}
