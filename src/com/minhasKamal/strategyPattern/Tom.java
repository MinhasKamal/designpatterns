package com.minhasKamal.strategyPattern;

public class Tom {
	public Tom(){
		super();
	}
	
	public void like(Expression e){
		e.execute();
	}
}
