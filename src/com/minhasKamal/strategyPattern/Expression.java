package com.minhasKamal.strategyPattern;

public abstract class Expression {
	public Expression(){
		super();
	}
	
	public abstract void execute();
}
