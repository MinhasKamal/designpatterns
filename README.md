# DESIGN PATTERNS
1.  Adapter Pattern
2.	Chain of Responsibility Pattern
3.	Composite Pattern
4.	Observer Pattern
5.	Singleton Pattern
6.	Strategy Pattern
7.	Template Pattern

# License
<a rel="license" href="http://www.gnu.org/licenses/gpl.html"><img alt="GNU General Public License" style="border-width:0" src="http://www.gnu.org/graphics/gplv3-88x31.png" /></a><br/>This project is licensed under a <a rel="license" href="http://www.gnu.org/licenses/gpl.html">GNU General Public License version-3</a>.
